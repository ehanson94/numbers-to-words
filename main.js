const onesName = ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
const teensName = ["", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
const tensName = ["", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]
const hundredsName = ["", "one hundred", "two hundred", "three hundred", "four hundred", "five hundred", "six hundred", "seven hundred", "eight hundred", "nine hundred"]

function addToPage(text) {
    const newDiv = document.createElement('div')
    newDiv.textContent = text
    list.appendChild(newDiv)
}

function numbersToWords() {
    for (let hundred = 0; hundred < hundredsName.length; hundred++) {
        for (let ten = 0; ten < tensName.length; ten++) {
            for (let one = 0; one < onesName.length; one++) {
                if (ten == 1 && one !== 0) {
                    addToPage(hundredsName[hundred] + ' ' + teensName[one])
                }
                else {
                    addToPage(hundredsName[hundred] + ' ' + tensName[ten] + ' ' + onesName[one])
                }
            }
        }
    }
    addToPage('one thousand')
}
numbersToWords()